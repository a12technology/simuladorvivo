<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Simulador de preços">
        <meta name="author" content="Alex junior">
        <title>Simulador Vivo</title>        
        <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/ie10-viewport-bug-workaround.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
    </head>

    <body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="javascript:void(0)">Simulador Vivo</a>
				</div>
			</div>
		</nav>

		<div class="container">
			<div class="row">
				<section class="col-md-8">
					<header>
						<h3>Simulador de Combos BC</h3>
					</header>

					<div class="containerAzul">
						<div class="tituloCombo">
							<h4 class="">TELEFONIA</h4>
						</div>
						<div class="containerSelect" id="containerSelect">
							<select class="form-control" name="selectOpcVoz" id="selectOpcVoz">
								@foreach($opcoesVoz as $idOpcaoVoz => $opcaoVoz)
									<option value="{{ $idOpcaoVoz }}">{{ $opcaoVoz }}</option>
								@endforeach
							</select>
							<span class="valorSelect" id="valorSelectVoz">R$ 0,00</span>
						</div>
						<div class="containerInfoValores">
							<table id="tabelaInfoVoz">
								<tbody>
									@foreach($arrayInfoVoz as $idInfoVoz => $infoVoz)
									<tr>
										<td>{{ $infoVoz['label'] }}</td>
										<td>{{ $infoVoz['valor'] }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div class="containerAzul">
						<div class="tituloCombo">
							<h4 class="">BANDA LARGA</h4>
						</div>
						<div class="containerSelect">
							<select class="form-control" name="selectOpcBLarga" id="selectOpcBLarga">
								@foreach($opcoesBLarga as $idOpcaoBLarga => $opcaoBLarga)
									<option value="{{ $idOpcaoBLarga }}">{{ $opcaoBLarga }}</option>
								@endforeach
							</select>
							<span class="valorSelect" id="valorSelectBLarga">R$ 0,00</span>
						</div>
						<div class="containerInfoValores">
							<table id="tabelaInfoBLarga">
								<tbody>
									@foreach($arrayInfoBandaLarga as $idInfoBandaLarga => $infoVozBandaLarga)
									<tr>
										<td>{{ $infoVozBandaLarga['label'] }}</td>
										<td>{{ $infoVozBandaLarga['valor'] }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div class="containerAzul">
						<div class="tituloCombo">
							<h4 class="">TV POR ASSINATURA</h4>
						</div>
						<div class="containerSelect">
							<select class="form-control" name="selectOpcTv" id="selectOpcTv">
								@foreach($opcoesTv as $idOpcaoTv => $opcaoTv)
									<option value="{{ $idOpcaoTv }}">{{ $opcaoTv }}</option>
								@endforeach
							</select>
							<span class="valorSelect" id="valorSelectTv">R$ 0,00</span>
						</div>
						<div class="containerInfoValores">
							<table id="pontosAdicionais">
								<tbody>
									@foreach($arrayInfoTv as $idInfoTv => $infoTv)
									<tr>
										<td colspan="2">{{ $infoTv['label'] }}</td>
										<td>{{ $infoTv['valor'] }}</td>
									</tr>
									@endforeach
									<tr>
										<td>
											<select class="form-control selectPontosAdicionais">
												<option value="1"></option>
												<option value="2">1</option>
												<option value="3">2</option>
												<option value="4">3</option>
												<option value="5">4</option>
												<option value="6">5</option>
											</select>
										</td>
										<td>Ponto Adicional</td>
										<td>R$ 0,00</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="containerAdicionaisTv">
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctColarTv" id="pctColarTv" value="pctColarTv">COLAR TV
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctEsportes" id="pctEsportes" value="pctEsportes">ESPORTES
								</label>
								<span id="valor_pctEsportes">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctInternacional" id="pctInternacional" value="pctInternacional">INTERNACIONAL
								</label>
								<span id="valor_pctInternacional">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctTelCineLightHd" id="pctTelCineLightHd" value="pctTelCineLightHd">TELECINE LIGHT HD
								</label>
								<span id="valor_pctTelCineLightHd">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctTelCineHd" id="pctTelCineHd" value="pctTelCineHd">TELECINE HD
								</label>
								<span id="valor_pctTelCineHd">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label id="">
									<input type="checkbox" class="opcAdicionaisTv" name="pctHboDigHd" id="pctHboDigHd" value="pctHboDigHd">HBO DIGITAL HD
								</label>
								<span id="valor_pctHboDigHd">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctHboMaxDigHd" id="pctHboMaxDigHd" value="pctHboMaxDigHd">HBO MAX DIGITAL HD
								</label>
								<span id="valor_pctHboMaxDigHd">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctCombate" id="pctCombate" value="pctCombate">COMBATE
								</label>
								<span id="valor_pctCombate">R$ 0,00</span>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" class="opcAdicionaisTv" name="pctSexyHot" id="pctSexyHot" value="pctSexyHot">SEXY HOT
								</label>
								<span id="valor_pctSexyHot">R$ 0,00</span>
							</div>
						</div>
					</div>
				</section>

				<section class="col-md-4">
					<div id="containerTotal" class="containerRoxo">
						<header>
							<h3>Total</h3>
							<div>Valor 1º mês referente 30 dias</div>
						</header>

						<div id="tabelaValoresTotal">
							<table class="table">
								<thead>
									<th></th>
									<th>Mensal</th>
									<th>Diário</th>
								</thead>
								<tbody>
									<tr>
										<td>1º ao 3º Mês</td>
										<td>R$ 0,00</td>
										<td>R$ 0,00</td>
									</tr>
									<tr>
										<td>4º ao 6º Mês</td>
										<td>R$ 0,00</td>
										<td>R$ 0,00</td>
									</tr>
									<tr>
										<td>7º ao 12º Mês</td>
										<td>R$ 0,00</td>
										<td>R$ 0,00</td>
									</tr>
									<tr>
										<td>após 12º Mês</td>
										<td>R$ 0,00</td>
										<td>R$ 0,00</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div id="containerInfoTotal">
							<table class="table">
								<tbody>
									<tr>
										<td>Total Taxa De Instalação</td>
										<td>R$ 0,00</td>
									</tr>
									<tr>
										<td>Total Combo + Taxas (1º Mês)</td>
										<td>R$ 0,00</td>
									</tr>
									<tr>
										<td>Total Preço Avulso</td>
										<td>R$ 0,00</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div id="containerGrupoPromocao">
						<div>
							<h3>GRUPO DE PROMOÇÕES</h3>
							<label id="labelGrupoPromocoes"></label>
						</div>
						<div>
							<h3>Campanha </h3>
							<label id="labelCampanha">sem Campanha</label>
							<br>
							<label id="laberColarTv"></label>
						</div>
					</div>
				</section>
			</div>
			<hr>
		</div>

        <script src="{{ asset('/js/jquery-1.12.2.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/script.js') }}"></script>

    </body>
</html>
