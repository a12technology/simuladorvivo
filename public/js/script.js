/**
 * Created by alex junior on 01/05/2016.
 * alexjunior.web@gmail.com | contato@alexjunior012.com
 */

$(function(){
	var arrayPctSelecionados = new Array();
	var plano = 'nenhum';
	var selectVoz = 1;
	var selectBLarga = 1;
	var selectTv = 1;
	var produtoSelecionado = 'nenhum';
	ciclo = 0;
	qtdProduto = 0;

	getPrecos();
	setTimeout(function(){
		valoresAvulso(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	}, 1000);

	$(".containerSelect select").change(function(){
		selectVoz	  = $('#selectOpcVoz').val();
		selectBLarga  = $('#selectOpcBLarga').val();
		selectTv	  = $('#selectOpcTv').val();

		if(selectVoz == 1  && selectBLarga == 1 && selectTv == 1){ plano = 'nenhum'; produtoSelecionado = 'nenhum'; }
		else if(selectVoz != 1 && selectBLarga == 1 && selectTv == 1){ plano = 'Avulso'; produtoSelecionado = 'voz'; }
		else if(selectVoz == 1 && selectBLarga != 1 && selectTv == 1){ plano = 'Avulso'; produtoSelecionado = 'bLarga'; }
		else if(selectVoz == 1 && selectBLarga == 1 && selectTv != 1){ plano = 'Avulso'; produtoSelecionado = 'tv'; }
		else if(selectVoz != 1 && selectBLarga != 1 && selectTv == 1){ plano = '2P'; produtoSelecionado = '2P1'; }
		else if(selectBLarga != 1  && selectVoz == 1 && selectTv != 1){ plano = '2P'; produtoSelecionado = '2P2'; }
		else if(selectVoz != 1 && selectBLarga == 1 && selectTv != 1){ plano = '2P'; produtoSelecionado = 'voz+tv'; }
		else if(selectVoz != 1 && selectBLarga != 1 && selectTv != 1){ plano = '3P'; produtoSelecionado = '3P'; }

		AplicaValor(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
		valoresPontosAdicionais(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado, $('select.selectPontosAdicionais').val() );
	});
	
	$("#selectOpcTv").change(function(){
		$("input[type=checkbox][class='opcAdicionaisTv']:checked").each(function(){
		    arrayPctSelecionados.push($(this).attr('id'));
		    pctSelecionado = $(this).attr('id');
		    valoresPctAdicionais(selectVoz, selectBLarga, selectTv, pctSelecionado, valoresPctAdicionais);
		});
		valoresPontosAdicionais(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado, $('select.selectPontosAdicionais').val() );
		valoresGrupoPromocao(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	});

	$("select.selectPontosAdicionais").change(function(){
		selectTv	  = $('#selectOpcTv').val();
		valoresPontosAdicionais(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado, $(this).val() );
		valoresGrupoPromocao(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	});

	$('input.opcAdicionaisTv').click(function(){
		pctSelecionado = $(this).attr('id');
		if(selectTv > 1){
			valoresPctAdicionais(selectVoz, selectBLarga, selectTv, pctSelecionado, valoresPctAdicionais);
		}
		valoresGrupoPromocao(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	});

});

function aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado){
	valoresTotais = arrayPrecos['arrayValoresTotais'];
	valoresPctAdd = arrayPrecos['arrayValoresTotais'];
	var valoresPlanosSelecionados = 0;
	var mensal_1_3 = 0;
	var mensal_4_6 = 0;
	var mensal_7_12 = 0;
	var apos_12_meses = 0;
	var taxaTotalInstalacao = 0;
	var somaPctAdicionais = 0;

	for (x in valoresTotais){
		if(arrayPrecos['arrayValoresTotais'][x] != 'Já Contém')
			mensal_1_3 += arrayPrecos['arrayValoresTotais'][x];
		else
			mensal_1_3 += 0;
	}
	mensal_1_3 = parseFloat(mensal_1_3);

	for (x in valoresPctAdd){
		if((x != 'valorComDescontoBLarga') && (x != 'valorComDescontoTv') && (x != 'valorComDescontoVoz') && (x != 'valorPontosAdicionais')){
			if(arrayPrecos['arrayValoresTotais'][x] != 'Já Contém')
				somaPctAdicionais += arrayPrecos['arrayValoresTotais'][x];
			else
				somaPctAdicionais += 0;
		} else {
			somaPctAdicionais += 0;
		}
	}

	for (x in arrayPrecos['arrayValoresPlanosSelecionados']){
		valoresPlanosSelecionados += arrayPrecos['arrayValoresPlanosSelecionados'][x];
	}

	valor_4_6 = (ciclo > 3 && ciclo <= 6)? arrayPrecos['arrayValoresTotais']['valorComDescontoBLarga']: ((ciclo > 6)? arrayPrecos['arrayValoresTotais']['valorComDescontoBLarga']: arrayPrecos['arrayValoresPlanosSelecionados']['valorBlarga'] );
	mensal_4_6 = arrayPrecos['arrayValoresTotais']['valorComDescontoTv']+valor_4_6+arrayPrecos['arrayValoresTotais']['valorComDescontoVoz']+arrayPrecos['arrayValoresTotais']['valorPontosAdicionais']+somaPctAdicionais;
	mensal_4_6 = parseFloat(mensal_4_6);

	valor_7_12 = (ciclo > 6 && ciclo <= 12)? arrayPrecos['arrayValoresTotais']['valorComDescontoBLarga']: arrayPrecos['arrayValoresPlanosSelecionados']['valorBlarga'];
	mensal_7_12 = arrayPrecos['arrayValoresTotais']['valorComDescontoTv']+valor_7_12+arrayPrecos['arrayValoresTotais']['valorComDescontoVoz']+arrayPrecos['arrayValoresTotais']['valorPontosAdicionais']+somaPctAdicionais;
	mensal_7_12 = parseFloat(mensal_7_12);

	apos_12_meses = valoresPlanosSelecionados+somaPctAdicionais+arrayPrecos['arrayValoresTotais']['valorPontosAdicionais'];
	apos_12_meses = parseFloat(apos_12_meses);

	comboTaxaMes = mensal_1_3+arrayPrecos['arrayTaxaInstalacao']['txInstalacaoTotal'];
	comboTaxaMes = parseFloat(comboTaxaMes);

	SomaAvulsa = arrayPrecos['voz']['Avulso'][selectVoz] + arrayPrecos['bLarga']['Avulso'][selectBLarga] + arrayPrecos['tv']['Avulso'][selectTv];
	TotalAvulso = SomaAvulsa+somaPctAdicionais+arrayPrecos['arrayValoresTotais']['valorPontosAdicionais'];
	TotalAvulso = parseFloat(TotalAvulso);

	// Aplica valores 1º ao 3º Mês
	$('#tabelaValoresTotal table tbody tr:nth-child(1) td:nth-child(2)').html('R$ '+ mensal_1_3.toFixed(2).replace('.', ',') );
	$('#tabelaValoresTotal table tbody tr:nth-child(1) td:nth-child(3)').html('R$ '+ (mensal_1_3/30).toFixed(2).replace('.', ',') );

	// Aplica Valores 4º ao 6º Mês
	$('#tabelaValoresTotal table tbody tr:nth-child(2) td:nth-child(2)').html('R$ '+mensal_4_6.toFixed(2).replace('.', ','));
	$('#tabelaValoresTotal table tbody tr:nth-child(2) td:nth-child(3)').html('R$ '+(mensal_4_6/30).toFixed(2).replace('.', ','));

	// Aplica Valores 7º ao 12º Mês
	$('#tabelaValoresTotal table tbody tr:nth-child(3) td:nth-child(2)').html('R$ '+mensal_7_12.toFixed(2).replace('.', ','));
	$('#tabelaValoresTotal table tbody tr:nth-child(3) td:nth-child(3)').html('R$ '+(mensal_7_12/30).toFixed(2).replace('.', ','));

	// Aplica Valores Apos 12º Mês
	$('#tabelaValoresTotal table tbody tr:nth-child(4) td:nth-child(2)').html('R$ '+apos_12_meses.toFixed(2).replace('.', ','));
	$('#tabelaValoresTotal table tbody tr:nth-child(4) td:nth-child(3)').html('R$ '+(apos_12_meses/30).toFixed(2).replace('.', ','));

	// Aplica Valores avulsos
	$('#containerInfoTotal table tr:nth-child(2) td:nth-child(2)').html('R$ '+comboTaxaMes.toFixed(2).replace('.', ','));
	$('#containerInfoTotal table tr:nth-child(3) td:nth-child(2)').html('R$ '+TotalAvulso.toFixed(2).replace('.', ','));
}

function AplicaValor(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado){
	arrayValoresPlanos = valoresPlanos(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	$('#valorSelectVoz').html('R$ '+arrayValoresPlanos['voz'].replace('.', ','));
	$('#valorSelectBLarga').html('R$ '+arrayValoresPlanos['bLarga'].replace('.', ','));
	$('#valorSelectTv').html('R$ '+arrayValoresPlanos['tv'].replace('.', ','));

	arrayTaxasInstalacao = valoresTaxaInstalacao(selectVoz, selectBLarga, selectTv, produtoSelecionado);
	$('#tabelaInfoVoz tr:nth-child(3) td:nth-child(2)').html('R$ '+ (arrayTaxasInstalacao['txInstalacaoVoz']).replace('.', ','));
	$('#tabelaInfoBLarga tr:nth-child(3) td:nth-child(2)').html('R$ '+ (arrayTaxasInstalacao['txInstalacaoBLarga']).replace('.', ','));
	$('#containerInfoTotal table tr:nth-child(1) td:nth-child(2)').html('R$ '+arrayTaxasInstalacao['txInstalacaoTotal'].toFixed(2).replace('.', ','));

	arrayValoresAvulsos = valoresAvulso(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	$('#tabelaInfoVoz tr:nth-child(2) td:nth-child(2)').html('R$ '+arrayValoresAvulsos['voz'].replace('.', ','));
	$('#tabelaInfoBLarga tr:nth-child(2) td:nth-child(2)').html('R$ '+arrayValoresAvulsos['bLarga'].replace('.', ','));
	$('#pontosAdicionais tr:nth-child(2) td:nth-child(2)').html('R$ '+arrayValoresAvulsos['tv'].replace('.', ','));

	arrayValoresDescontos = valoresDesconto(selectVoz, selectBLarga, selectTv);
	$('#tabelaInfoVoz tr:nth-child(4) td:nth-child(2)').html('R$ '+arrayValoresDescontos['voz'].toFixed(2).replace('.', ','));
	$('#tabelaInfoBLarga tr:nth-child(4) td:nth-child(2)').html('R$ '+arrayValoresDescontos['bLarga'].toFixed(2).replace('.', ','));
	$('#pontosAdicionais tr:nth-child(3) td:nth-child(2)').html('R$ '+arrayValoresDescontos['tv'].toFixed(2).replace('.', ','));

	$('#tabelaInfoVoz tr:nth-child(1) td:nth-child(2)').html('R$ '+(arrayValoresPlanos['voz']-arrayValoresDescontos['voz']).toFixed(2).replace('.', ','));
	$('#tabelaInfoBLarga tr:nth-child(1) td:nth-child(2)').html('R$ '+(arrayValoresPlanos['bLarga']-arrayValoresDescontos['bLarga']).toFixed(2).replace('.', ','));
	$('#pontosAdicionais tr:nth-child(1) td:nth-child(2)').html('R$ '+(arrayValoresPlanos['tv']-arrayValoresDescontos['tv']).toFixed(2).replace('.', ','));

	arrayPrecos['arrayValoresTotais']['valorComDescontoVoz'] = (arrayValoresPlanos['voz']-arrayValoresDescontos['voz']);
	arrayPrecos['arrayValoresTotais']['valorComDescontoBLarga'] = (arrayValoresPlanos['bLarga']-arrayValoresDescontos['bLarga']);
	arrayPrecos['arrayValoresTotais']['valorComDescontoTv'] = (arrayValoresPlanos['tv']-arrayValoresDescontos['tv']);
	
	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	valoresGrupoPromocao(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado);
}

function valoresPlanos(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado){
	var arrayValores = [];

	if(plano == 'Avulso'){
		arrayValores['voz'] = arrayPrecos['voz'][plano][selectVoz].toFixed(2);
		arrayValores['bLarga'] = arrayPrecos['bLarga'][plano][selectBLarga].toFixed(2);
		arrayValores['tv'] = arrayPrecos['tv'][plano][selectTv].toFixed(2);
	} else if(plano == '2P'){
		if(produtoSelecionado == '2P1' || produtoSelecionado == '2P2'){
			arrayValores['voz'] = arrayPrecos['voz'][plano][selectVoz].toFixed(2);
			arrayValores['bLarga'] = arrayPrecos['bLarga'][plano][produtoSelecionado][selectBLarga].toFixed(2);
			arrayValores['tv'] = arrayPrecos['tv'][plano][selectTv].toFixed(2);
		} else if(produtoSelecionado == 'voz+tv'){
			arrayValores['voz'] = arrayPrecos['voz']['Avulso'][selectVoz].toFixed(2);
			arrayValores['bLarga'] = arrayPrecos['bLarga']['Avulso'][selectBLarga].toFixed(2);
			arrayValores['tv'] = arrayPrecos['tv'][plano][selectTv].toFixed(2);
		}
	} else if(plano == '3P'){
		arrayValores['voz'] = arrayPrecos['voz'][plano][selectVoz].toFixed(2);
		arrayValores['bLarga'] = arrayPrecos['bLarga'][plano][selectBLarga].toFixed(2);
		arrayValores['tv'] = arrayPrecos['tv'][plano][selectTv].toFixed(2);
	} else {
		arrayValores['voz'] = arrayPrecos['voz']['Avulso'][selectVoz].toFixed(2);
		arrayValores['bLarga'] = arrayPrecos['bLarga']['Avulso'][selectBLarga].toFixed(2);
		arrayValores['tv'] = arrayPrecos['tv']['Avulso'][selectTv].toFixed(2);
	}

	arrayPrecos['arrayValoresPlanosSelecionados']['valorVoz'] = parseFloat(arrayValores['voz']);
	arrayPrecos['arrayValoresPlanosSelecionados']['valorBlarga'] = parseFloat(arrayValores['bLarga']);
	arrayPrecos['arrayValoresPlanosSelecionados']['valorTv'] = parseFloat(arrayValores['tv']);

	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	return arrayValores;
}

function valoresTaxaInstalacao(selectVoz, selectBLarga, selectTv, produtoSelecionado){
	var txInstalacaoVoz = txInstalacaoBLarga = 0.0;
	var arrayTaxas = [];

	if(selectVoz > 1 && selectBLarga == 1 && selectTv==1){
		txInstalacaoVoz = 119.50;
	} else{
		txInstalacaoVoz = 0.00;
	}

	if(selectVoz == 1 && selectBLarga > 1 && selectTv==1){
		txInstalacaoBLarga = 240.00;
	} else{
		txInstalacaoBLarga = 0.00;
	}

	arrayTaxas['txInstalacaoVoz'] = txInstalacaoVoz.toFixed(2);
	arrayTaxas['txInstalacaoBLarga'] = txInstalacaoBLarga.toFixed(2);
	arrayTaxas['txInstalacaoTotal'] = txInstalacaoVoz+txInstalacaoBLarga;

	arrayPrecos['arrayTaxaInstalacao']['txInstalacaoVoz'] = txInstalacaoVoz.toFixed(2);
	arrayPrecos['arrayTaxaInstalacao']['txInstalacaoBLarga'] = txInstalacaoBLarga.toFixed(2);
	arrayPrecos['arrayTaxaInstalacao']['txInstalacaoTotal'] = txInstalacaoVoz+txInstalacaoBLarga;

	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	return arrayTaxas;
}

function valoresAvulso(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado){
	var arrayValorAvulso = [];

	arrayValorAvulso['voz'] = arrayPrecos['voz']['Avulso'][selectVoz].toFixed(2);
	arrayValorAvulso['bLarga'] = arrayPrecos['bLarga']['Avulso'][selectBLarga].toFixed(2);
	arrayValorAvulso['tv'] = arrayPrecos['tv']['Avulso'][selectTv].toFixed(2);

	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);
	return arrayValorAvulso;
}

function valoresDesconto(selectVoz, selectBLarga, selectTv){
	var arrayDesconto = [];
	arrayDesconto['voz'] = 0.00;
	arrayDesconto['bLarga'] = 0.00;
	arrayDesconto['tv'] = 0.00;

	if(selectVoz == 1 && selectBLarga == 1 && (selectTv == 2 || selectTv == 3)){
		arrayDesconto['tv'] = 10.00;
	} else if(selectVoz == 1 && selectBLarga == 1 && (selectTv == 4 || selectTv == 5)){
		arrayDesconto['tv'] = 20.00;
	} else if(selectVoz == 1 && selectBLarga == 1 && (selectTv == 6 || selectTv == 7)){
		arrayDesconto['tv'] = 0.00;
	} else if(selectVoz == 1 && selectBLarga == 1 && selectTv == 10){
		arrayDesconto['tv'] = 10.00;
	} else if(selectVoz > 1 && selectBLarga == 2 && selectTv == 1){
		arrayDesconto['bLarga'] = 40.0;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 2 || selectTv == 3)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 10.00;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv >= 4 && selectTv <= 7)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 0.00;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 8 || selectTv == 9)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 25.00;
	} else if(selectVoz == 1 && selectBLarga == 3 && selectTv == 10){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 10.00;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 2 || selectTv == 3)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 10.00;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 4 || selectTv == 5)){
		arrayDesconto['bLarga'] = 40.0;
		if( $("select.selectPontosAdicionais").val() == 1 ){
			arrayDesconto['tv'] = 20.00;
		} else {
			arrayDesconto['tv'] = 25.00;
		}
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 6 || selectTv == 7)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 0.00;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 8 || selectTv == 9)){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 25.00;
	} else if(selectVoz > 1 && selectBLarga == 3 && selectTv == 10){
		arrayDesconto['bLarga'] = 40.0;
		arrayDesconto['tv'] = 10.00;
	} else {
		arrayDesconto['voz'] = 0.00;
		arrayDesconto['bLarga'] = 0.00;
		arrayDesconto['tv'] = 0.00;
	}

	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, '');
	return arrayDesconto;
}

function valoresPontosAdicionais(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado, qtdSelecionada){
	var valor = 0.0;
	var valorPontoAdicional = 0.0;

	AplicaValor(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado)

	if(selectVoz == 1  && selectBLarga == 1 && selectTv == 1){ qtdProduto = 0; }

	else if(selectVoz != 1 && selectBLarga == 1 && selectTv == 1){ qtdProduto = 1; }
	else if(selectVoz == 1 && selectBLarga != 1 && selectTv == 1){ qtdProduto = 1; }
	else if(selectVoz == 1 && selectBLarga == 1 && selectTv != 1){ qtdProduto = 1; }

	else if(selectVoz != 1 && selectBLarga != 1 && selectTv == 1){ qtdProduto = 2; }
	else if(selectVoz != 1 && selectBLarga == 1 && selectTv != 1){ qtdProduto = 2; }
	else if(selectVoz == 1 && selectBLarga != 1 && selectTv != 1){ qtdProduto = 2; }
	
	else if(selectVoz != 1 && selectBLarga != 1 && selectTv != 1){ qtdProduto = 3; }

	if(qtdSelecionada > 1)
		valorPontoAdicional = (selectTv > 1)? arrayPrecos['pontosAdicionais'][qtdProduto][selectTv] : 0;
	else
		valorPontoAdicional = 0;

	var varApoio = (qtdSelecionada > 1)? 2: 1;

	if(qtdSelecionada > 1){
		if(qtdProduto == 1){
			valor = (qtdSelecionada-1)*valorPontoAdicional;
		} else {
			if(selectBLarga>1 && (selectTv == 2 || selectTv == 3)){
				valor =  (qtdSelecionada-1)*valorPontoAdicional;
			} else if(selectBLarga>1 && (selectTv == 4 || selectTv == 5)){
				valor = ((qtdSelecionada-varApoio) == 0)? valorPontoAdicional : ((qtdSelecionada-varApoio)*29.9)+valorPontoAdicional;
			} else if(selectBLarga>1 && (selectTv == 6 || selectTv == 7)){
				valor = ((qtdSelecionada-varApoio) == 0)? valorPontoAdicional : ((qtdSelecionada-varApoio)*29.9)+valorPontoAdicional;				
			} else if(selectBLarga>1 && (selectTv == 8 || selectTv == 9)){
				valor = ((qtdSelecionada-varApoio) == 0)? valorPontoAdicional : ((qtdSelecionada-varApoio)*29.9)+valorPontoAdicional;
			} else if(selectBLarga>1 && selectTv == 10){
				valor = ((qtdSelecionada-varApoio) == 0)? valorPontoAdicional : ((qtdSelecionada-varApoio)*29.9)+valorPontoAdicional;
			} else {
				valor = 0;
			}
		}
	} else {
		valor = 0;
	}

	arrayPrecos['arrayValoresTotais']['valorPontosAdicionais'] = valor;
	$('#pontosAdicionais tr:nth-child(4) td:nth-child(3)').html('R$ '+valor.toFixed(2).replace('.', ','));
	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);

}

function valoresGrupoPromocao(plano, selectVoz, selectBLarga, selectTv, produtoSelecionado){
	var grupoPromocao = '<br>';
	var Campanha = 'Sem Campanha';
	var ColarTv = '<br>';
	ciclo = 0;

	if(selectVoz == 1 && selectBLarga > 1 && selectTv == 1){
		grupoPromocao = 'BL SOLO';
		Campanha = 'IND - 1P - ABR16 - VIVO FIBRA2 (50,100, 200 E 300MB)';
		ColarTv = '<br>';
		ciclo = 0;
	} else if (selectVoz == 1 && selectBLarga == 1 && (selectTv == 2 || selectTv == 3)){
		grupoPromocao = 'TV SOLO';
		Campanha = 'IND - 1P - MAI16 - VIVO FIBRA TV (SUPERHD / SUPERHDFUT)';
		ColarTv = '<br>';
		ciclo = 0;
	} else if (selectVoz == 1 && selectBLarga == 1 && (selectTv == 4 || selectTv == 5)){
		grupoPromocao = 'TV SOLO';
		Campanha = 'IND - 1P - MAI16 - VIVO FIBRA TV (ULTRAHD / ULTRAHDFUT)';
		ColarTv = '<br>';
		ciclo = 0;
	} else if (selectVoz == 1 && selectBLarga == 1 && (selectTv == 6 || selectTv == 7)){
		grupoPromocao = 'TV SOLO';
		Campanha = 'IND - 1P - MAI16 - VIVO FIBRA TV (ULTIMATEHD/ ULTIMATEHDFUT)';
		ColarTv = '<br>';
		ciclo = 0;
	} else if (selectVoz == 1 && selectBLarga == 1 && selectTv == 10){
		grupoPromocao = 'TV SOLO';
		Campanha = 'IND - 1P - MAI16 - VIVO FIBRA TV (FULLHD / FULLHDFUT)';
		ColarTv = '<br>';
		ciclo = 0;
	} else if(selectVoz > 1 && selectBLarga == 2 && selectTv == 1){
		grupoPromocao = 'BL DUO';
		Campanha = 'IND - 2P - ABR16 - VIVO FIBRA (50, 100, 200, 300MB)';
		ColarTv = '<br>';
		ciclo = 3;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 2 || selectTv == 3)){
		grupoPromocao = 'DUO';
		Campanha = 'IND - 2P - MAI16 - VIVO FIBRA BL + TV (SUPERHD / SUPERHDFUT)';
		ColarTv = 'IND - 2P - MAI16 TV (SUPER HD/ SUPER HD FUT) + BL';
		ciclo = 3;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 4 || selectTv == 5)){
		grupoPromocao = 'DUO';
		Campanha = 'IND - 2P - MAI16 - VIVO FIBRA BL + TV (ULTRAHD / ULTRAHDFUT)';
		ColarTv = 'IND - 2P - MAI16 TV (ULTRA HD/ ULTRA HD FUT) + BL';
		ciclo = 3;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 6 || selectTv == 7)){
		grupoPromocao = 'DUO';
		Campanha = 'IND - 2P - MAI16 - VIVO FIBRA BL+TV(ULTIMATEHD/UTIMATEHDFUT)';
		ColarTv = 'IND - 2P - MAI16 TV (ULTIMATE HD/ULTIMATE HD FUT) + BL';
		ciclo = 3;
	} else if(selectVoz == 1 && selectBLarga == 3 && selectTv == 10){
		grupoPromocao = 'DUO';
		Campanha = 'IND - 2P - MAI16 - VIVO FIBRA BL+TV (FULLHD / FULLHD FUT)';
		ColarTv = 'IND - 2P - MAI16 TV (FULLHD/ FULLAHD FUT) + BL';
		ciclo = 3;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 2 || selectTv == 3)){
		grupoPromocao = 'TRIO';
		Campanha = 'IND - 3P - MAI16 - VIVO FIBRA BL + TV (SUPERHD / SUPERHDFUT)';
		ColarTv = 'IND - 3P - MAI16 TV (SUPER HD/ SUPER HD FUT) + BL';
		ciclo = 12;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 4 || selectTv == 5)){
		grupoPromocao = 'TRIO';
		Campanha = 'IND - 3P - MAI16 - VIVO FIBRA BL + TV (ULTRAHD / ULTRAHDFUT)';
		ColarTv = 'IND - 3P - MAI16 TV (ULTRA HD/ ULTRA HD FUT)';
		ciclo = 12;
	} else if(selectVoz > 1 && selectBLarga == 3 && (selectTv == 6 || selectTv == 7)){
		grupoPromocao = 'TRIO';
		Campanha = 'IND - 3P - MAI16 -VIVO FIBRA BL+TV(ULTIMATEHD/ULTIMATEHDFUT)';
		ColarTv = 'IND - 3P - MAI16 TV (ULTIMATE HD/ULTIMATE HD FUT) + BL';
		ciclo = 12;
	} else if(selectVoz > 1 && selectBLarga == 3 &&  (selectTv == 8 || selectTv == 9)){
		grupoPromocao = 'TRIO S/PRV';
		Campanha = 'IND-3P-MAI16 - FIBRA BL+TV(ULTIMATEHD/ULTIMATEHDFUT) SEM PVR';
		ColarTv = 'IND - 3P - MAI16 TV (ULTIMATE HD/ULTIMATE HD FUT) S/ PVR +BL';
		ciclo = 12;
	} else if(selectVoz > 1 && selectBLarga == 3 && selectTv == 10){
		grupoPromocao = 'TRIO';
		Campanha = 'IND - 3P - MAI16 - VIVO FIBRA BL+TV (FULLHD / FULLHD FUT)';
		ColarTv = 'IND - 3P - MAI16 TV (FULLHD/ FULLAHD FUT) + BL';
		ciclo = 12;
	} else if(selectVoz == 1 && selectBLarga == 3 && (selectTv == 8 || selectTv == 9)){
		grupoPromocao = 'DUO S/PVR';
		Campanha = 'IND-2P-MAI16 - FIBRA BL+TV(ULTIMATEHD/ULTIMATEHDFUT) SEM PVR';
		ColarTv = 'IND - 2P - MAI16 TV (ULTIMATE HD/ULTIMATE HD FUT) S/ PVR +BL';
		ciclo = 12;
	} else {
		grupoPromocao = '';
		Campanha = 'Sem Campanha';
		ColarTv = '<br>';
		ciclo = 0;
	}

	$('#labelGrupoPromocoes').html(grupoPromocao);
	$('#labelCampanha').html(Campanha);

	if( $("#pctColarTv:checked").length == 0 ){
		$('#laberColarTv').html('<br>');
	} else {
		$('#laberColarTv').html(ColarTv);
	}
}


function valoresPctAdicionais(selectVoz, selectBLarga, selectTv, pctSelecionado, produtoSelecionado){
	var valor = 0.00;

	if(selectTv > 1 && selectTv < 6){

		if( $("#" + pctSelecionado + ":checked").length == 0 ){
			valor = valor;
		} else {
			valor = arrayPrecos['pctAdicionais'][pctSelecionado];
		}

	} else if(selectTv >= 6 && selectTv < 10){

		if( $("#" + pctSelecionado + ":checked").length == 0 ){
			valor = valor;
		} else {
			if(pctSelecionado == 'pctEsportes' || pctSelecionado == 'pctInternacional'){
				valor = 'Já Contém';
			} else {
				valor = arrayPrecos['pctAdicionais'][pctSelecionado];
			}
		}
	} else if(selectTv == 10){
		if( $("#" + pctSelecionado + ":checked").length == 0 ){
			valor = valor;
		} else {
			if(pctSelecionado == 'pctCombate' || pctSelecionado == 'pctSexyHot'){
				valor = arrayPrecos['pctAdicionais'][pctSelecionado];
			} else {
				valor = 'Já Contém';
			}
		}
	} else {
		valor = valor;
	}

	arrayPrecos['arrayValoresTotais'][pctSelecionado] = valor;
	valorTratado = (valor == 'Já Contém')? valor: 'R$ '+valor;
	$('#valor_'+pctSelecionado).html( (valorTratado.replace('.', ',')) );

	aplicaValorFinal(arrayPrecos, selectVoz, selectBLarga, selectTv, produtoSelecionado);

}

function getPrecos(){
	$.ajax({
        url: 'precosPlano',
        dataType: 'json',
        type: "GET",
        cache: false,
        data: '',
        quietMillis: 250
    }).done(function(data){
    	arrayPrecos = data
    }).fail(function(){
        return false
    });
}