<?php

namespace simuladorVivo\Http\Controllers;

class PrecosController extends Controller
{
	public function getPrecosVoz()
	{
		$arrrayPrecosVoz = [
			'Avulso' => [
				1 => 0.00,
				2 => 33.80,
				3 => 49.90,
				4 => 59.90,
				5 => 69.90,
				6 => 79.90,
			],
			'2P' => [
				1 => 0.00,
				2 => 33.80,
				3 => 49.90,
				4 => 59.90,
				5 => 69.90,
				6 => 79.90,
			],
			'3P' => [
				1 => 0.00,
				2 => 33.80,
				3 => 49.90,
				4 => 59.90,
				5 => 69.90,
				6 => 79.90,
			]
		];
		return $arrrayPrecosVoz;
	}

	public function getPrecosBLarga()
	{
		$arrrayBLarga = [
			'Avulso' => [
				1 => 0.00,
				2 => 119.90,
				3 => 129.90,
				4 => 179.90,
				5 => 229.90,
			],
			'2P' => [
				'2P1' => [
					1 => 0.00,
					2 => 99.90,
					3 => 109.90,
					4 => 159.90,
					5 => 209.90,
				],
				'2P2' => [
					1 => 0.00,
					2 => 99.90,
					3 => 99.90,
					4 => 149.90,
					5 => 199.90,
				],
			],
			'3P' => [
				1 => 0.00,
				2 => 99.90,
				3 => 99.90,
				4 => 149.90,
				5 => 199.90,
			],
		];
		return $arrrayBLarga;
	}

	public function getPrecosTv()
	{
		$arrayTv = [
			'Avulso' => [
				1 => 0.00,
				2 => 119.90,
				3 => 199.90,
				4 => 149.90,
				5 => 229.90,
				6 => 189.90,
				7 => 269.90,
				8 => 189.90,
				9 => 269.90,
				10 => 259.90,
				11 => 339.90,
			],
			'2P' => [
				1 => 0.00,
				2 => 89.90,
				3 => 169.90,
				4 => 119.90,
				5 => 199.90,
				6 => 159.90,
				7 => 239.90,
				8 => 159.90,
				9 => 239.90,
				10 => 229.90,
				11 => 309.90,
			],
			'3P' => [
				1 => 0.00,
				2 => 89.90,
				3 => 169.90,
				4 => 119.90,
				5 => 199.90,
				6 => 159.90,
				7 => 239.90,
				8 => 159.90,
				9 => 239.90,
				10 => 229.90,
				11 => 309.90,

			],
		];

		return $arrayTv;
	}

	public function getPrecosPctAdicionais()
	{
		$arrayPctAdicionais = [
			'pctColarTv' => 0,
			'pctEsportes' => 19.90,
			'pctInternacional' => 19.90,
			'pctTelCineLightHd' => 24.90,
			'pctTelCineHd' => 39.90,
			'pctHboDigHd' => 19.90,
			'pctHboMaxDigHd' => 29.90,
			'pctCombate' => 62.90,
			'pctSexyHot' => 43.90,
		];
		return $arrayPctAdicionais;
	}

	public function getPrecosPontosAdicionais(){
		$arrayPontosAdicionais = [
			1 => [
				1 => 0.00,
				2 => 29.90,
				3 => 29.90,
				4 => 29.90,
				5 => 29.90,
				6 => 29.90,
				7 => 29.90,
				8 => 29.90,
				9 => 29.90,
				10 => 29.90,

			],
			2 => [
				1 => 0.00,
				2 => 29.90,
				3 => 29.90,
				4 => 9.90,
				5 => 9.90,
				6 => 0.00,
				7 => 0.00,
				8 => 0.00,
				9 => 0.00,
				10 => 0.00,
			],
			3 => [
				1 => 0.00,
				2 => 29.90,
				3 => 29.90,
				4 => 9.90,
				5 => 9.90,
				6 => 0.00,
				7 => 0.00,
				8 => 0.00,
				9 => 0.00,
				10 => 0.00,
			],
		];
		return $arrayPontosAdicionais;
	}

	public function getValoresTotais()
	{
		$arrayValoresTotais = [
			'valorComDescontoVoz' => 0.00,
			'valorComDescontoBLarga' => 0.00,
			'valorComDescontoTv' => 0.00,
			'valorPontosAdicionais' => 0.00,
			'pctEsportes' => 0.00,
			'pctInternacional' => 0.00,
			'pctTelCineLightHd' => 0.00,
			'pctTelCineHd' => 0.00,
			'pctHboDigHd' => 0.00,
			'pctHboMaxDigHd' => 0.00,
			'pctCombate' => 0.00,
			'pctSexyHot' => 0.00,
			'pctColarTv' => 0.00,
		];
		return $arrayValoresTotais;
	}

	public function getValoresPlanosSelecionados()
	{
		$arrayValoresPlanosSelecionados = [
			'valorVoz' => 0.00,
			'valorBlarga' => 0.00,
			'valorTv' => 0.00,
		];
		return $arrayValoresPlanosSelecionados;
	}

	public function getTaxasInstalacao()
	{
		$arrayTaxaInstalacao = [
			'txInstalacaoVoz' => 0.00,
			'txInstalacaoBLarga' => 0.00,
			'txInstalacaoTotal' => 0.00,
		];
		return $arrayTaxaInstalacao;
	}

	public function getArrayCampanhas()
	{
		$arrayCampanhas = [
			'GrupoPromocoes' => '',
			'Campanha' => '',
		];
		return $arrayCampanhas;
	}

}































