<?php

namespace simuladorVivo\Http\Controllers;

use simuladorVivo\Http\Controllers\PrecosController;

class HomeController extends Controller
{
	public function index()
	{		
		return view('Layout.home', [
				'opcoesVoz' => $this->getVoz(),
				'opcoesBLarga' => $this->getBandaLarga(),
				'opcoesTv' => $this->getTv(),
				'arrayInfoVoz' => $this->getInfoVoz(),
				'arrayInfoBandaLarga' => $this->getInfoBandaLarga(),
				'arrayInfoTv' => $this->getInfoTv(),
			]);
	}

	private function getVoz()
	{
		$arrayOpcoesVoz = [
			1 => '-- não selecionado --',
			2 => 'Vivo Fixo Simples',
			3 => 'Ilimitado Fixo Local',
			4 => 'Ilimitado Fixo e Móvel Local',
			5 => 'Ilimitado Fixo Brasil e Móvel Local'
		];

		return $arrayOpcoesVoz;
	}

	private function getBandaLarga()
	{
		$arrayOpcoesBlarga = [
			1 => '-- não selecionado --',
			2 => '50 Mbps',
			3 => '100 Mbps',
			4 => '200 Mbps',
			5 => '300 Mbps'
		];

		return $arrayOpcoesBlarga;
	}

	private function getTv()
	{
		$arrayOpcoesTv = [
			1 => '-- não selecionado --',
			2 => 'SUPER HD',
			3 => 'SUPER HD FUTEBOL',
			4 => 'ULTRA HD',
			5 => 'ULTRA HD FUTEBOL',
			6 => 'ULTIMATE HD',
			7 => 'ULTIMATE HD FUTEBOL',
			8 => 'ULTIMATE HD- S PRV',
			9 => 'ULTIMATE HD FUTEBOL- S PRV',
			10 => 'FULL HD ',
		];

		return $arrayOpcoesTv;
	}

	private function getInfoVoz()
	{
		$arrayInfoVoz = [
			1 => ['label'=>'Valor COM desconto', 'valor'=>'R$ 0,00'],
			2 => ['label'=>'Avulso:', 'valor'=>'R$ 0,00'],
			3 => ['label'=>'Tx. Instalação:', 'valor'=>'R$ 0,00'],
			4 => ['label'=>'Desconto:', 'valor'=>'R$ 0,00'],
		];

		return $arrayInfoVoz;
	}

	private function getInfoBandaLarga()
	{
		$arrayInfoBandaLarga = [
			1 => ['label'=>'Valor COM desconto', 'valor'=>'R$ 0,00'],
			2 => ['label'=>'Avulso:', 'valor'=>'R$ 0,00'],
			3 => ['label'=>'Tx. Instalação:', 'valor'=>'R$ 0,00'],
			4 => ['label'=>'Desconto:', 'valor'=>'R$ 0,00'],
		];
		
		return $arrayInfoBandaLarga;
	}

	private function getInfoTv()
	{
		$arrayInfoTv = [
			1 => ['label'=>'Valor COM desconto', 'valor'=>'R$ 0,00'],
			2 => ['label'=>'Avulso:', 'valor'=>'R$ 0,00'],
			3 => ['label'=>'Desconto:', 'valor'=>'R$ 0,00'],
		];
		
		return $arrayInfoTv;
	}

	private function getPontosAdicionais()
	{
		return [1, 2, 3, 4, 5];
	}

	private function getPacotesAdicionais()
	{
		$arrayPagotes = [
			'COLAR TV',
			'ESPORTES',
			'INTERNACIONAL',
			'TELECINE LIGHT HD',
			'TELECINE HD',
			'HBO DIGITAL HD',
			'HBO MAX DIGITAL HD',
			'COMBATE',
			'SEXY HOT',
		];
	}

	public function getPrecos()
	{
		$oPrecosController = new PrecosController();

		return [
			'voz' => $oPrecosController->getPrecosVoz(),
			'bLarga' => $oPrecosController->getPrecosBLarga(),
			'tv' => $oPrecosController->getPrecosTv(),
			'pctAdicionais' => $oPrecosController->getPrecosPctAdicionais(),
			'pontosAdicionais' => $oPrecosController->getPrecosPontosAdicionais(),
			'arrayValoresTotais' => $oPrecosController->getValoresTotais(),
			'arrayTaxaInstalacao' => $oPrecosController->getTaxasInstalacao(),
			'arrayCampanhas' => $oPrecosController->getArrayCampanhas(),
			'arrayValoresPlanosSelecionados' => $oPrecosController->getValoresPlanosSelecionados(),
		];
	}

}